#![allow(unused_parens)]

use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};
use std::thread;

pub fn calc_pi_sequentially(iterations: u32) -> f64 {
    let mut points_in_cq = 0;

    for _ in 0..iterations {
        let x = rand::random::<f32>();
        let y = rand::random::<f32>();

        if(is_point_in_circle_quadrant(x,y)){
            points_in_cq += 1;
        }
    }

    let ratio: f64 = points_in_cq as f64 / iterations as f64;
    return ratio * 4.0;
}

pub fn calc_pi_channels(iterations: u32) -> f64 {
    let mut total_points = 0;
    let iters_per_part = iterations / 5;
    let mut recvs: Vec<std::sync::mpsc::Receiver<u32>> = Vec::new();

    for _ in 0..5 {
        recvs.push(spawn_calc_thread(iters_per_part));
    }

    for i in 0..5 {
        total_points += recvs[i].recv().unwrap();
    }

    let ratio: f64 = total_points as f64 / iterations as f64;

    return ratio * 4.0;
}

pub fn calc_pi_channels_one_receiver(iterations: u32) -> f64 {
    let mut total_points = 0;
    let iters_per_part = iterations / 5;

    let (tx, rx) = mpsc::channel();

    for _ in 0..5 {
        spawn_calc_thread_no_receiver(iters_per_part, tx.clone());
    }

    for _ in 0..5 {
        let p = rx.recv().unwrap();
        total_points += p;
    }

    let ratio: f64 = total_points as f64 / iterations as f64;

    return ratio * 4.0;
}

fn spawn_calc_thread(iters: u32) -> Receiver<u32> {
    let (tx, rx) = mpsc::channel();
    thread::spawn(move || {
        let points_in_cq = get_points_count(iters);

        tx.send(points_in_cq).unwrap();
    });
    return rx;
}

fn spawn_calc_thread_no_receiver(iters: u32, tx: Sender<u32>) -> () {
    thread::spawn(move || {
        let points_in_cq = get_points_count(iters);

        tx.send(points_in_cq).unwrap();
    });
}

fn get_points_count(iters: u32) -> u32 {
    let mut points_in_cq: u32 = 0;
    for _ in 0..iters {
        let x = rand::random::<f32>();
        let y = rand::random::<f32>();
        if(is_point_in_circle_quadrant(x,y)){
            points_in_cq += 1;
        }
    }

    return points_in_cq;
}

fn is_point_in_circle_quadrant(x: f32, y: f32) -> bool {
    if(x*x + y*y < 1.0) { return true } else { return false };
}
#![allow(unused_parens)]
use std::env;

mod pi_approximator;

fn main() {
    const ITERATIONS: u32 = 100000000;
    let args : Vec<String> = env::args().collect();
    if(args.len() < 2){
        print_usage();
        return;
    }

    println!("Starting");
    let mut x: f64 = 0.0;

    if(args[1] == "-s"){
        x = pi_approximator::calc_pi_sequentially(ITERATIONS);
    }
    else if(args[1] == "-p"){
        x = pi_approximator::calc_pi_channels(ITERATIONS);
    }
    else if(args[1] == "-p1"){
        x = pi_approximator::calc_pi_channels_one_receiver(ITERATIONS);
    }
    else {
        print_usage();
        return;
    }

    println!("Pi is {}", x);
}

fn print_usage(){
    println!("Please run with -s (sequential) or -p (parallel) option");
}